## Assignment 4

I included both code examples in this one project as they do not interfere with each other. 

### Topic 1 -  Task scheduling
I changed the file `app/Console/Kernel.php` and added multiple schedules. The first schedule uses the Log Facade and saves a log message with different flags every minute. The second schedule runs every saturday at 8 pm and logs the corresponding time and weekday. Lastly I added a job, that clears the application cache on a daily basis.

### Topic 2 -  Storage
For this topic I added two routes to the `web.php` file. The first route `public_upload` saves an uploaded file using the `public` disk. As soon as the file was stored, a link appears on the homepage that directs the user to the publicly accessible file.
The second route `private_upload` does the same as the first route but it uses the `local` disk instead. After uploading the user is presented with a link to the location of the file. However, as the `local` disk was used to save the file, the server response with a 404.