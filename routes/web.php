<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function() {
    $show_public_link = count(Storage::files('public/uploads/')) > 0;
    $show_private_link = count(Storage::files('private_uploads/')) > 0;
    return view('welcome')->with(['show_public_link' => $show_public_link, 'show_private_link' => $show_private_link]);
});

Route::post('/public_upload', function(Request $request){
    Storage::disk('public')->putFileAs('uploads', $request->file('file'), 'last_public_upload.jpg');
    return redirect('/');
})->name('public_upload');

Route::post('/private_upload', function(Request $request){
    Storage::disk('local')->putFileAs('private_uploads', $request->file('file'), 'last_private_upload.jpg');
    return redirect('/');
})->name('private_upload');
