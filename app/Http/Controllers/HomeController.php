<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');//->with('somedata', 42);
    }

    public function update(Request $request) {
        $id = Auth::id();
        $user = User::find($id);
        $user->name = $request->name;
        $user->save();
        return redirect('/home')->with('status', 'User with id ' . $id .  ' changed his/her name to ' . $user->name);
    }
    
    public function dumps() {
        $x = time();
        dump($x);
        return "Function continued";
    }
}
