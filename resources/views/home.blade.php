@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>{{ Auth::user()->name }}</td>
                            <td>{{ Auth::user()->email }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <form action="/update" method="POST">
                        @csrf
                        <input type="text" style="display: block;" name="name" placeholder="new user name">
                        <button class="btn btn-primary mt-2" type="submit">Update name</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
