<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            <h3>Upload using the <em>public</em> disk</h3>
            <form action="{{route('public_upload')}}" enctype="multipart/form-data" method="post">
                @csrf
                <input type="file" name="file" id="file">
                <button type="submit">Upload</button>
            </form>
            @if($show_public_link)
            <a target="_blank" href="{{asset('storage/uploads/last_public_upload.jpg')}}">File is saved here and accessible</a>
            @endif


            <h3>Upload using the <em>private</em> disk</h3>
            <form action="{{route('private_upload')}}" enctype="multipart/form-data" method="post">
                @csrf
                <input type="file" name="file" id="file">
                <button type="submit">Upload</button>
            </form>
            @if($show_private_link)
            <a target="_blank" href="{{asset('storage/private_uploads/last_private_upload.jpg')}}">File is saved here, but not accessible</a>
            @endif
            </div>
    </body>
</html>
